<?php
header("Access-Control-Allow-Origin:  http://localhost:8080");
header("Access-Control-Allow-Headers: Content-Type");

require __DIR__ . '/vendor/autoload.php';

use app\Models\Router;
use app\Controllers\ProductController;

$router = new Router();

$router->get('/products', [ProductController::class, 'getProducts']);
$router->post('/add', [ProductController::class, 'addProduct']);
$router->post('/delete', [ProductController::class, 'deleteProduct']);


$router->resolve();



