<?php

namespace app\Interfaces;

interface RouteInterface
{
    /**
     * @param $url
     * @param $fn
     *
     * @return mixed
     */
   public function get($url, $fn);

    /**
     * @param $url
     * @param $fn
     *
     * @return mixed
     */
   public function post($url, $fn);

    /**
     * @return mixed
     */
   public function render404();

    /**
     * @return mixed
     */
   public function resolve();
}