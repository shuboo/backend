<?php

namespace app\Controllers;
use app\Models\Validation;
use app\Models\Book;
use app\Models\Furniture;
use app\Models\Dvd;

class ProductController
{
    /**
     * @return string
     */
   public function getProducts (): string
   {
        $products = [];
        $dvd = new Dvd();
        $furniture = new Furniture();
        $book = new Book();
        array_push($products, ...$dvd->get());
        array_push($products, ...$furniture->get());
        array_push($products, ...$book->get());

        return json_encode($products);
    }

    /**
     * @param $request
     * @return false|string
     */
    public function addProduct($request)
    {
        $productType = 'app\Models\\' . ucfirst($request['type']);
        if (file_exists($productType . '.php')) {
            $product = new $productType();
            $validate = new Validation($request, $product->validation_array);
            if ($validate->isError()) {
                return json_encode([
                    "message" => $validate->errors,
                    "code" => 400,
                ]);
            }
            $product->add($request);
        }
        return json_encode([
            "message" => "Product successfully added",
            "code" => 200,
        ]);
    }

    /**
     * @param $request
     * @return false|string
     */
    public function deleteProduct($request)
    {
        $skus = implode("', '", array_values($request));
        $dvd = new Dvd();
        $book = new Book();
        $furniture = new Furniture();
        $dvd->delete($skus);
        $book->delete($skus);
        $furniture->delete($skus);

        return json_encode([
            "message" => "Product deleted",
            "code" => 200,
        ]);
    }
}

