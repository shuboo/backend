<?php

namespace app\Models;

abstract class Products
{
    protected $sku;
    protected $name;
    protected $price;
    protected Database $database;

    protected function __construct()
    {
        $this->database = new Database();
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku): void
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    abstract public function get();

    /**
     * @param $request
     *
     * @return mixed
     */
    abstract public function add($request);

    /**
     * @param $skus
     */
    public function delete($skus)
    {
        $statement = $this->database->pdo->prepare("DELETE FROM " . $this->table_name . " WHERE sku in ('$skus')");
        $statement->execute();
    }
}