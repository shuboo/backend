<?php

namespace app\Models;

use PDO;

class Dvd extends Products
{
    protected $size;
    public string $table_name = 'dvd';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size): void
    {
        $this->size = $size;
    }

    /**
     * @return array|false
     */
    public function get()
    {
        $statement = $this->database->pdo->prepare('SELECT * FROM '. $this->table_name);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $request
     *
     * @return void
     */
    public function add($request)
    {
        $this->setSku($request['sku']);
        $this->setName($request['name']);
        $this->setPrice($request['price']);
        $this->setSize($request['size']);
        $statement = $this->database->pdo->prepare("INSERT INTO " . $this->table_name . "(sku, name, price, size) VALUES (:sku, :name, :price, :size)");
        $statement->bindValue(':sku', $this->getSku());
        $statement->bindValue(':name', $this->getName());
        $statement->bindValue(':size', $this->getSize());
        $statement->bindValue(':price', $this->getPrice());


        $statement->execute();
    }

    /**
     * @var array|string[]
     */
    public array $validation_array = [
        'sku' => 'string',
        'price' => 'integer',
        'name' => 'string',
        'type' => 'string',
        'size' => 'integer'
    ];




}