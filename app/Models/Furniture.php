<?php

namespace app\Models;

use PDO;

class Furniture extends Products
{
    protected $height;
    protected $width;
    protected $length;
    public string $table_name = 'furniture';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height): void
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width): void
    {
        $this->width = $width;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param mixed $length
     */
    public function setLength($length): void
    {
        $this->length = $length;
    }

    /**
     * @return array|false
     */
    public function get()
    {
        $statement = $this->database->pdo->prepare('SELECT * FROM '. $this->table_name);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $request
     * @return void
     */
    public function add($request)
    {
        $this->setSku($request['sku']);
        $this->setName($request['name']);
        $this->setPrice($request['price']);
        $this->setLength($request['length']);
        $this->setWidth($request['width']);
        $this->setHeight($request['height']);
        $statement = $this->database->pdo->prepare("INSERT INTO " . $this->table_name . "(sku, name, price, length, width, height) 
                VALUES (:sku, :name, :price, :length, :width, :height)");
        $statement->bindValue(':sku', $this->getSku());
        $statement->bindValue(':name', $this->getName());
        $statement->bindValue(':length', $this->getLength());
        $statement->bindValue(':price', $this->getPrice());
        $statement->bindValue(':width', $this->getWidth());
        $statement->bindValue(':height', $this->getHeight());


        $statement->execute();
    }

    /**
     * @var array|string[]
     */
    public array $validation_array = [
        'sku' => 'string',
        'price' => 'integer',
        'name' => 'string',
        'type' => 'string',
        'length' => 'integer',
        'width' => 'integer',
        'height' => 'integer'
    ];


}