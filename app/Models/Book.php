<?php

namespace app\Models;

use PDO;

class Book extends Products
{
    protected $weight;
    public string $table_name = 'book';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param $weight
     */
    public function setWeight($weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return array|false
     */
    public function get()
    {
        $statement = $this->database->pdo->prepare('SELECT * FROM '. $this->table_name);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $request
     * @return void
     */
    public function add($request)
    {
        $this->setSku($request['sku']);
        $this->setName($request['name']);
        $this->setPrice($request['price']);
        $this->setWeight($request['weight']);
        $statement = $this->database->pdo->prepare("INSERT INTO " . $this->table_name . "(sku, name, price, weight) VALUES (:sku, :name, :price, :weight)");
        $statement->bindValue(':sku', $this->getSku());
        $statement->bindValue(':name', $this->getName());
        $statement->bindValue(':weight', $this->getWeight());
        $statement->bindValue(':price', $this->getPrice());

        $statement->execute();
    }

    /**
     * @var array|string[]
     */
    public array $validation_array = [
        'sku' => 'string',
        'price' => 'integer',
        'name' => 'string',
        'type' => 'string',
        'weight' => 'integer'
    ];


}