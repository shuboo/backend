<?php

namespace app\Models;
use PDO;

class Validation
{
    private array $data;
    public array $errors = [];
    private array $fields = [];
    protected \app\Models\Database $database;


    /**
     * @param $request
     * @param $validation_array
     */
    public function __construct($request, $validation_array)
    {
        $this->data = $request;
        $this->fields = $validation_array;
        $this->database = new Database();
        $this->validateProduct();
        $this->validateSku();
    }

    public function validateProduct()
    {
        foreach ($this->fields as $key => $value)
        {
            if (!isset($this->data[$key]))
            {
                $this->addErrors($key,"Please submit required data of $key!");
            } else
            {
                $this->validateType($key, $value);
            }
        }
    }

    private function validateType($key, $value)
    {
        if ($value == "integer") {
            if (!is_numeric($this->data[$key])) {
                $this->addErrors($key,"Please provide the data of indicated type!");
            }
        } else {
            if (gettype($this->data[$key]) !== $value) {
                $this->addErrors($key,"Please provide the data of indicated type!");
            }
        }
    }

    private function findSku($product): bool
    {
        $sku = $this->data['sku'];
        $statement = $this->database->pdo->prepare('SELECT * FROM '. $product->table_name ." WHERE sku='$sku'");
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        if (count($result) > 0) {
            return true;
        }
        return false;
    }

    private function validateSku()
    {
        if (!isset($this->data['sku'])) {
            return ;
        }
        $key = $this->data['sku'];
        $dvd = new Dvd();
        $book = new Book();
        $furniture = new Furniture();

        if ($this->findSku($dvd) || $this->findSku($book) || $this->findSku($furniture)) {
            $this->addErrors($key,"Product with same sku!");
        }
    }

    private function addErrors($key, $value)
    {
        if (!key_exists($key, $this->errors)) {
        $this->errors[$key] = $value; }
    }

    public function isError(): bool
    {
        return count($this->errors) != 0;
    }
}