<?php
namespace app\Models;

use app\Interfaces\RouteInterface;

class Router implements RouteInterface
{
    protected const NOT_FOUND_MESSAGE = 'Not Found';

    /**
     * @var array
     */
    protected array $getRoutes = [];

    /**
     * @var array
     */
    protected array $postRoutes = [];

    /**
     * @param $url
     * @param $fn
     *
     * @return void
     */
    public function get($url, $fn)
    {
        $this->getRoutes[$url] = $fn;
    }

    /**
     * @param $url
     * @param $fn
     *
     * @return void
     */
    public function post($url, $fn)
    {
        $this->postRoutes[$url] = $fn;
    }

    public function render404(): array
    {
        return [
            'message' => self::NOT_FOUND_MESSAGE,
            'code' => 404
        ];
    }

    /**
     * @return array|void
     */
    public function resolve()
    {
        $url = $_SERVER['PATH_INFO'] ?? '/';
        $method = strtolower($_SERVER['REQUEST_METHOD']);
        $request = file_get_contents('php://input');
        $request = json_decode($request, true);


        if ($method == 'get') {
            $fn = $this->getRoutes[$url] ?? false;
        } else {
            $fn = $this->postRoutes[$url] ?? false;
        }
        if (!$fn) {
            return $this->render404();
        }
      echo  call_user_func_array($fn, $request);
    }
}