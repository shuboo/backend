<?php

namespace app\Models;

use PDO;

class Database
{
    protected string $host ="localhost";
    protected string $user ="root";
    protected string $pwd ="";
    protected string $dbname ="products_list";
    public PDO $pdo;


    public function __construct()
    {
        $dsn = 'mysql:host=' . $this->host . ";" . 'dbname=' . $this->dbname;
        $pdo = new PDO($dsn, $this->user, $this->pwd );
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo = $pdo;
    }
}